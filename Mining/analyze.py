import Riot_Const as const

def UPDATE(player_list,winrate_list):
	winrate_list = MatchWithUpdate(player_list,winrate_list)
	winrate_list = WinMatchWithUpdate(player_list,winrate_list)
	winrate_list = MatchAgainstUpdate(player_list,winrate_list)
	winrate_list = WinMatchAgainstUpdate(player_list,winrate_list)
	return winrate_list
def MatchWithUpdate(player_list,winrate_list):
	champlist_0=[player_list[0],player_list[1],player_list[2],player_list[3],player_list[4]]
	champlist_1=[player_list[5],player_list[6],player_list[7],player_list[8],player_list[9]]
	champlist_0.sort()# store team_0's champs into a list and sort to create pair combinations
	champlist_1.sort()# store team_1's champs into a list and sort to create pair combinations
	for j in range (0,5):# Count match with of team_0
		for k in range(j,5):
			id=Id(champlist_0[j],champlist_0[k])
			winrate_list[id][1]=winrate_list[id][1]+1 #winrate_list[1]= match with
	for j in range (0,5):# Count match with of team_1
		for k in range(j,5):
			id=Id(champlist_1[j],champlist_1[k])
			winrate_list[id][1]=winrate_list[id][1]+1
	return winrate_list
	
def WinMatchWithUpdate(player_list,winrate_list):
	if player_list[10]==0 :# RESULT
		champlist=[player_list[0],player_list[1],player_list[2],player_list[3],player_list[4]]
		champlist.sort()# store team_0's champs into a list and sort to create pair combinations
		for j in range (0,5):
			for k in range(j,5):
				id=Id(champlist[j],champlist[k])
				winrate_list[id][2]=winrate_list[id][2]+1# winrate_list[3]= match win with
	if player_list[10]==1: # RESULT
		champlist=[player_list[5],player_list[6],player_list[7],player_list[8],player_list[9]]
		champlist.sort()# store team_1's champs into a list and sort to create pair combinations
		for j in range (0,5):
			for k in range(j,5):
				id=Id(champlist[j],champlist[k])
				winrate_list[id][2]=winrate_list[id][2]+1 # winrate_list[3]= match win with
	return winrate_list	

def MatchAgainstUpdate(player_list,winrate_list):
	for j in range (0,5):# Choose 1 champ of team_0
		for k in range(5,10):#Choose 1 champ of team_1
			champlist=[player_list[j],player_list[k]]
			champlist.sort()
			id=Id(champlist[0],champlist[1])
			winrate_list[id][3]=winrate_list[id][3]+1# winrate_list[0]= match against
	return winrate_list

def WinMatchAgainstUpdate(player_list,winrate_list):
	if player_list[10]==0 :# RESULT team_0 win
		for j in range (0,5):# Choose 1 champ of team_0
			for k in range(5,10):#Choose 1 champ of team_1
				champlist=[player_list[j],player_list[k]]
				if champlist[0]<champlist[1]:# NEU A WIN B VA A.id<B.id THI MOI CAP NHAT VAO` LIST DO TRONG LIST CHI CO A.id<B.id!!!
					id=Id(champlist[0],champlist[1])
					winrate_list[id][4]=winrate_list[id][4]+1# winrate_list[0]= match against
	if player_list[10]==1 :# RESULT team_1 win
		for j in range (5,10):# Choose 1 champ of team_1
			for k in range(0,5):#Choose 1 champ of team_0
				champlist=[player_list[j],player_list[k]]
				if champlist[0]<champlist[1]:# NEU A WIN B VA A.id<B.id THI MOI CAP NHAT VAO` LIST DO TRONG LIST CHI CO A.id<B.id!!!
					id=Id(champlist[0],champlist[1])
					winrate_list[id][4]=winrate_list[id][4]+1# winrate_list[0]= match against
	return winrate_list

def Id(a,b):
	return int((a-1)*(const.number_of_champions-a/2+1)+b-a)#id of each pair in total_match & total win match list

