from RiotAPI import RiotAPI
import sqlite3 as sql 
import time
import Riot_Const as const

def main():
	keyPlayerID = 227488010
	keyPlayerName = 'Nixim'
	key = 0
	api = RiotAPI(const.API_KEY['keys'][0])
	conn = sql.connect('test_table.db')
	c = conn.cursor()
	c.execute('CREATE TABLE IF NOT EXISTS samplePlayer (name TEXT PRIMARY KEY, accountID INTEGER)')	
	command = 'REPLACE INTO samplePlayer(name, accountID) VALUES (\'' + keyPlayerName + '\',' + str(keyPlayerID) + ')'
	c.execute(command)
	r = api.get_match_list_by_playerId (str(keyPlayerID))
	count = 1
	pointer = 0
	start_time = time.time()
	print(r['totalGames'])
	while(pointer < r['endIndex'] and count < 1000):
		matchID = r['matches'][pointer]['gameId']
		game = api.get_match_by_matchId(matchID)
		if('status' in game and game['status']['status_code'] == 429):
			key = changeKey(key)
			api = RiotAPI(const.API_KEY['keys'][key])
			game = api.get_match_by_matchId(matchID)
		if('status' not in game or game['status']['status_code'] != 404):
			for one in game['participantIdentities']:
				if(one['player']['currentAccountId'] != keyPlayerID):
					command = 'REPLACE INTO samplePlayer(name, accountID) VALUES (\'' + one['player']['summonerName'] + '\',' + str(one["player"]["accountId"]) + ')'
					c.execute(command)
					count = count + 1
					print(count)
			pointer = pointer + 1
	conn.commit()
	c.close()
	conn.close()

# Change the key code being used 
def changeKey(key):
	key = key + 1 
	if(key == const.API_KEY['number_of_key']):
		key = 0
	return key

if __name__ == "__main__":
	main()	