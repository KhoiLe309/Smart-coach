from RiotAPI import RiotAPI
import sqlite3 as sql 
import time
import Riot_Const  as const


def main():
    key = 0 
    region = const.REGIONS['North_America']
    samplePlayerNames = ('God diOs Deus', 'a wood', 'K 18 9 19', 'Anivia Bòt', 'code no worky', 'moelisreject', 'ShäDE', 'elreight', 'Mistaken420', 'She Hates You')
    api = RiotAPI(const.API_KEY['keys'][key], region)
    name_Id = sql.connect('Database/player_na1.db')
    nameId_table = name_Id.cursor()
    nameId_table.execute('CREATE TABLE IF NOT EXISTS nameId(playerID INTEGER PRIMARY KEY, playerName TEXT)')
    #solve problem get 100 matchID of samplePlayerName(use accountId to get all match)
    for samplePlayerName in samplePlayerNames:
        player = api.get_summoner_by_name(samplePlayerName)
        if('status' in player and player['status']['status_code'] == 429):          
                key = changeKey(key, const.API_KEY['number_of_key'])
                api = RiotAPI(const.API_KEY['keys'][key], region)
                player = api.get_summoner_by_name(samplePlayerName)
        if('status' not in player or player['status']['status_code'] != 404):
	        playerID = player['accountId']
	        match_list = api.get_match_list_by_playerId(playerID)
	        if('status' in match_list and match_list['status']['status_code'] == 429):          
	                key = changeKey(key, const.API_KEY['number_of_key'])
	                api = RiotAPI(const.API_KEY['keys'][key], region)
	                match_list = api.get_match_list_by_playerId(playerID)
	        #In 100 matches of samplePlayerName, we get playerName and PlayerID   
	        if('status' not in match_list or match_list['status']['status_code'] != 404):
	            index = 0
	            while (index < match_list['endIndex']):
	                queue_type = match_list['matches'][index]['queue']   # Type of the queue
	                if(queue_type in (440, 420, 430)):   # get rank match (5v5)
	                    matchId = match_list['matches'][index]['gameId']
	                    game = api.get_match_by_matchId(matchId)
	                    if ('status' in game and game['status']['status_code'] == 429): 
	                        key = changeKey(key, const.API_KEY['number_of_key'])  
	                        api = RiotAPI(const.API_KEY['keys'][key], region)
	                        game = api.get_match_by_matchId(matchId)            
	                    #save playerName and playerId to NameId_data.db
	                    if('status' not in game or game['status']['status_code'] != 404):
		                    for i in range(10):
		                        command = 'REPLACE INTO nameId(playerID, playerName) VALUES('
		                        playerName = game['participantIdentities'][i]['player']['summonerName']
		                        playerId = game['participantIdentities'][i]['player']['accountId']
		                        command = command + str(playerId) + ',' + '\'' + str(playerName) + '\'' + ')'
		                        name_Id.execute(command)
	                index = index + 1

    name_Id.commit()
    nameId_table.close()
    name_Id.close()

def changeKey(key, number_of_key):
    key = key + 1 
    if(key == number_of_key):
        key = 0
    return key

if __name__ == '__main__':
    main()
