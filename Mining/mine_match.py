from RiotAPI import RiotAPI
import sqlite3 as sql 
import Riot_Const  as const


# Main method 
def main():
	key = 0   # Key pointer use for looping through the key
	api = RiotAPI(const.API_KEY['keys'][key])   # Initialize the RIOT_API module 
	match_data = sql.connect('matches_data.db')   # Connect matches_data.db 
	match_table = match_data.cursor()
	player_data = sql.connect('Database/test_table.db')   # Connect test_table.db
	player_table = player_data.cursor()
	# Create table for storing the match data
	match_table.execute('CREATE TABLE IF NOT EXISTS matches(gameID INTEGER pr, firstChamp INTEGER, secondChamp INTEGER, thirdChamp INTEGER, forthChamp INTEGER, fifthChamp INTEGER, sixthChamp INTEGER, seventhChamp INTEGER, eigthChamp INTEGER, ninthChamp INTEGER, tenthCghamp INTEGER, winner INTEGER)')
	player_table.execute('SELECT accountID FROM samplePlayer')   # Get player data and store into a list
	players = player_table.fetchall()   # Get all player data from the test_table.db
	count = 0   # counter to counter the data got input to the database
	big_counter = 0   # pointer for looping over the player
	while(count < 1000):
		player_id = players[big_counter][0]   
		match_list = api.get_match_list_by_playerId(player_id)
		if('status' in match_list and match_list['status']['status_code'] == 429):  		# Check if the rate limit is reached, change the key then request again
			key = changeKey(key)
			api = RiotAPI(const.API_KEY['keys'][key])
			match_list = api.get_match_list_by_playerId(player_id)
		if('status' not in match_list or match_list['status']['status_code'] != 404):  		# Check if the data is existed or not, if not, skip
			pointer = 0   #pointer to loop through the history of player
			while(pointer < match_list['endIndex']):   # Loop through the matches history of the player 
				queue_type = match_list['matches'][pointer]['queue']   # Type of the queue
				if(queue_type in (440, 420)):    # Execute if and only if it is match making rank game
					match_id = match_list['matches'][pointer]['gameId']
					game = api.get_match_by_matchId(match_id)   
					if ('status' in game and game['status']['status_code'] == 429):	  # Check if the rate limit is reached, change the key then request again
						key = changeKey(key)
						api = RiotAPI(const.API_KEY['keys'][key])
						game = api.get_match_by_matchId(match_list['matches'][pointer]['gameId'])
					command = 'REPLACE INTO matches VALUES(' + str(game['gameId'])   # Command to save match data into sql file
					for one in game['participants']:	# Loop through participant to get the champions participate in the match 
						lane_id = const.LANE_ID[one['timeline']['lane']]
						champion_id = const.CHAMPION_ID_MAP[one['championId']]
						command = command + ',' + str(champion_id) + str(lane_id)
					winner = 0   # Indicator of who wins the match
					if (game['teams'][0]['win'] == 'Win'):
						winner = 0
					else:
						winner = 1
					command = command + ',' + str(winner) + ')'
					match_data.execute(command)    # Execute the command 
					count = count + 1
					print(count)
				pointer = pointer + 1
		big_counter = big_counter + 1
	# CLosing the database to prevent data leak
	match_data.commit()
	match_table.close()
	match_data.close()
	player_data.commit()
	player_table.close()
	player_data.close()

# Change the key code being used 
def changeKey(key):
	key = key + 1 
	if(key == const.API_KEY['number_of_key']):
		key = 0
	return key

if __name__ == '__main__':
	main()

