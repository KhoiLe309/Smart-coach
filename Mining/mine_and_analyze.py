from RiotAPI import RiotAPI
import Riot_Const as const
import sqlite3 as sql
import analyze

def main():
	#Setup
	key =  0
	region = const.REGIONS['Europe_West']
	number_of_champions = 141
	game_version = const.game_version
	pair_database = sql.connect('Database/8_12.db')   
	pair_data = pair_database.cursor()
	player_database = sql.connect('Database/player_euw.db')
	player_data = player_database.cursor()
	api = RiotAPI(const.API_KEY['keys'][key], region)

	
	pair_data.execute('CREATE TABLE IF NOT EXISTS pair(pair_code INTEGER PRIMARY KEY, match_with INTEGER, win_with INTEGER, match_against INTEGER, win_against INTEGER)')
	for i in range (1, number_of_champions + 1):
		for j in range (i, number_of_champions + 1):
			pair_code = i * 1000 + j
			pair_data.execute('REPLACE INTO pair(pair_code, match_with, win_with, match_against, win_against) VALUES(' + str(pair_code) + ',0,0,0,0)')
	
	pair_data.execute('SELECT * FROM pair')
	player_data.execute('SELECT playerID FROM nameId')
	player_tuple = pair_data.fetchall()
	pair_table = [list(item) for item in player_tuple]
	player_table = player_data.fetchall()
	player_table_length = len(player_table)
	player_counter = 0
	
	try: 
		while (player_counter < player_table_length):
			player_id = player_table[player_counter][0]
			match_list = api.get_match_list_by_playerId(player_id)
			if('status' in match_list and match_list['status']['status_code'] == 429):  		# Check if the rate limit is reached, change the key then request again
				key = changeKey(key)
				api = RiotAPI(const.API_KEY['keys'][key], region)
				match_list = api.get_match_list_by_playerId(player_id)
			if('status' not in match_list or match_list['status']['status_code'] != 404):  	    # Check if the data is existed or not, if not, skip
				last_game_patch = False
				match_list_counter = 0
				while( (not last_game_patch) and (match_list_counter < match_list['endIndex'])):
					queue_type = match_list['matches'][match_list_counter]['queue']   # Type of the queue
					if(queue_type in (440, 420)):    # Execute if and only if it is match making rank game
						match_id = match_list['matches'][match_list_counter]['gameId']
						game = api.get_match_by_matchId(match_id)   
						if ('status' in game and game['status']['status_code'] == 429):	  # Check if the rate limit is reached, change the key then request again
							key = changeKey(key)
							api = RiotAPI(const.API_KEY['keys'][key], region)
							game = api.get_match_by_matchId(match_id)	
						if('status' not in game or game['status']['status_code'] != 404):
							game_version_length = len(game_version)
							if(game['gameVersion'][0:game_version_length] == game_version):
								game_data = []
								for one in game['participants']:	# Loop through participant to get the champions participate in the match 
									champion_id = const.CHAMPION_ID_MAP[one['championId']]
									game_data.append(champion_id)
								winner = 0   # Indicator of who wins the match
								if (game['teams'][0]['win'] == 'Win'):
									winner = 0
								else:
									winner = 1	
								game_data.append(winner)
								pair_table = analyze.UPDATE(game_data, pair_table)
							else:
								last_game_patch = True
					match_list_counter = match_list_counter + 1
			player_counter = player_counter + 1
	except KeyboardInterrupt :
		pass
	except KeyError:
		pass

	print(player_counter)
	for pair in pair_table: 
		command = 'REPLACE INTO pair(pair_code, match_with, win_with, match_against, win_against) VALUES(' + str(pair[0]) + ',' + str(pair[1]) + ',' + str(pair[2]) + ',' + str(pair[3]) + ',' + str(pair[4]) + ')' 
		pair_data.execute(command)

	player_database.commit()
	player_data.close()
	player_database.close()

	pair_database.commit()
	pair_data.close()
	pair_database.close()

# Change the key code being used 
def changeKey(key):
	key = key + 1 
	if(key == const.API_KEY['number_of_key']):
		key = 0
	return key

if __name__ == '__main__':
	main()