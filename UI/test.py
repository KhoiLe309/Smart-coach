from tkinter import *
from PIL import ImageTk
from PIL import Image
class PlayerChamp:

	def create_champ_window(self):
		self.champ_window = Tk()

	def __init__(self, master):

		self.master = master
		self.frame = Frame(self.master, width = 30, height =10, bg = "blue")
		self.frame.pack()
		self.summonerIcon = Button(self.frame, width = 10, height = 4)
		self.summonerIcon.pack(side = LEFT)
		self.textFrame = Frame(self.frame, bg = "red")
		self.textFrame.pack(side = LEFT)
		self.champName = Label(self.textFrame, text = "Darius", fg = "white", bg = "blue", width = 40, height = 2)
		self.champName.pack(side = TOP, fill = Y)
		self.champType = Label(self.textFrame, text = "Tank", fg = "white", bg = "blue", width = 40, height = 2)
		self.champType.pack(side = TOP ,fill = Y )
		self.image = Image.open("Darius.PNG")
		self.image = self.image.resize((80, 80), Image.ANTIALIAS)
		self.champicon = ImageTk.PhotoImage(self.image)
		self.summonerIcon.config(image = self.champicon, width = 80, height = 80)
		self.create_champ_window()
root = Tk()
app = PlayerChamp(root)
root.mainloop()